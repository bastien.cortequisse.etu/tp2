import data from './data.js';
import Component from './components/Component.js';
import Img from './components/Img.js';

// D. Manipulation des chaînes

// const name = 'Regina';
// const url = `images/${name.toLowerCase()}.jpg`;
// const html = `
// 	<article class="pizzaThumbnail">
// 		<a href="${url}">
// 			<img src="${url}" />
// 			<section>${name}</section>
// 		</a>
// 	</article>
// `;
// document.querySelector('.pageContent').innerHTML = html;

// // E.1. Manipulation des tableaux
// let data = [ 'Regina', 'Napolitaine', 'Spicy' ];
// html = '';
// // E.1. boucle for :
// for (let i = 0; i < data.length; i++) {
// 	const nom = data[i],
// 		url = `images/${nom.toLowerCase()}.jpg`;
// 	html += `<article class="pizzaThumbnail">
// 		<a href="${url}">
// 			<img src="${url}" />
// 			<section>${nom}</section>
// 		</a>
// 	</article>`;
// }
// document.querySelector('.pageContent').innerHTML = html;

// E.1. Array.forEach :
// html = '';
// data.forEach( nom => {
// 	const url = `images/${nom.toLowerCase()}.jpg`;
// 	html += `<a href="${url}">
// 		<img src="${url}" />
// 		<h4>${nom}</h4>
// 	</a>`;
// })
// document.querySelector('.pageContent').innerHTML = html;

// // E.1. Array.map + Array.join :
// html = data.map( nom => `<a href="images/${nom.toLowerCase()}.jpg">
// <img src="images/${nom.toLowerCase()}.jpg" />
// <h4>${nom}</h4>
// </a>`).join('');
// document.querySelector('.pageContent').innerHTML = html;

// // E.1. Array.reduce :
// html = data.reduce( (str, nom) => str+`<a href="images/${nom.toLowerCase()}.jpg">
// 	<img src="images/${nom.toLowerCase()}.jpg" />
// 	<h4>${nom}</h4>
// </a>`,'');
// document.querySelector('.pageContent').innerHTML = html;

// E.2. Les objets littéraux

const title = new Component('h1', null, 'La carte');
document.querySelector('.pageTitle').innerHTML = title.render();

const img = new Img(
	'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
);
document.querySelector('.pageContent').innerHTML = img.render();

/*class Character {
	firstName;
	lastName;
	constructor(firstName, lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	fullName() {
		return `${this.firstName} ${this.lastName}`;
	}
}

const heisenberg = new Character('Walter', 'White');
console.log(
	heisenberg.firstName,
	heisenberg.fullName(),
);*/

/*function render(pizzas) {
	const html = pizzas.reduce( (str, {image, name, price_small, price_large}) => str +
		`<article class="pizzaThumbnail">
			<a href="${image}">
				<img src="${image}" />
				<section>
					<h4>${name}</h4>
					<ul>
						<li>Prix petit format : ${price_small.toFixed(2)} €</li>
						<li>Prix grand format : ${price_large.toFixed(2)} €</li>
					</ul>
				</section>
			</a>
		</article>`, '');
	document.querySelector('.pageContent').innerHTML = html;
}

// G.1. Tri de tableaux
function sortByName(a, b) {
	if ( a.name < b.name ) {
		return -1;
	} else if ( a.name > b.name ) {
		return 1;
	}
	return 0;
}

// tri par price_small croissant PUIS par price_large croissant
function sortByPrice(a, b) {
	if ( a.price_small !== b.price_small ) {
		return a.price_small - b.price_small;
	}
	return a.price_large - b.price_large;
}

// // Attention : data.sort() modifie le tableau original
render( data );
// render( data.sort(sortByName) );
// render( data.sort(sortByPrice) );

// // G.2. Système de filtre
// render( data.filter( pizza => pizza.base == 'tomate' ))
// render( data.filter( pizza => pizza.price_small < 6 ))
// // deux possibilités pour les pizzas avec deux fois la lettre "i"
// render( data.filter( pizza => pizza.name.split('i').length == 3 ))
// render( data.filter( pizza => pizza.name.match(/i/g).length == 2 ))

// // G.3. Destructuring
// render( data.filter( ({base}) => base == 'tomate' ))
// render( data.filter( ({price_small}) => price_small < 6 ))
// render( data.filter( ({name}) => name.match(/i/g).length === 2 ));
*/
