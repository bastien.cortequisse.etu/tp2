import Component from './Component.js';
export default class Img extends Component {
	constructor(valueImg) {
		super('img', { name: 'src', value: valueImg }, null);
	}
}
