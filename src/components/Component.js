export default class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		if (this.attribute == null) {
			return `<${this.tagName}>${this.children}</${this.tagName}>`;
		} else if (this.children != null) {
			return `<${this.tagName} ${this.attribute.name}='${this.attribute.value}'>${this.children}</${this.tagName}>`;
		} else {
			return `<img ${this.attribute.name}='${this.attribute.value}' />`;
		}
	}
}
